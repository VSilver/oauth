from django.db import models


class User(models.Model):
    email = models.CharField(max_length=100)
    name_surname = models.CharField(max_length=50)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.name + ' ' + self.surname


class App(models.Model):
    id = models.CharField(max_length=10, unique=True, primary_key=True)

    def __str__(self):
        return self.id


class Session(models.Model):
    token = models.CharField(max_length=10)
    user = models.ForeignKey(User, related_name='sessions')
    time = models.DateTimeField(editable=True, auto_now=True)
    app = models.ForeignKey(App, related_name='sessions')

    def __str__(self):
        return self.user.email + ' ' + self.token
