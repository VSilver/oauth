import json
import random as rand
import datetime
import string
from django.http import JsonResponse, HttpResponse
from authentication.models import *


def index(request):
    context_dict = {}
    return HttpResponse("Index page.")


def register(request):
    context_dict = {'out': {}}
    print(request.GET)
    if request.method == 'GET':
        # data = request.body.decode("utf-8").replace('\'', '')
        data = list(request.GET)
        if data == []:
            data = request.body.decode("utf-8").replace('\'', '')
        else:
            data = data[0].replace('\'', '')
        try:
            parsed = json.loads(data)
            app_id = parsed['app_id']
            email = parsed['email']
            name_surname = parsed['name_surname']
            password = parsed['password']
            if check_app_id(app_id):
                if not check_user_existence(email):
                    token = generate_token()
                    context_dict['out']['token'] = token
                    context_dict['out']['code'] = 0
                    user = save_new_user_to_db(email, name_surname, password)
                    create_session(user, token, app_id)
                else:
                    context_dict['out']['code'] = 1
            else:
                context_dict['out']['code'] = 2
        except ValueError:
            print("Decoding JSON failed.")
            context_dict['out']['code'] = 2
    else:
        context_dict['out']['code'] = 2
    return JsonResponse(context_dict)


def login(request):
    context_dict = {'out': {}}
    if request.method == 'GET':
        data = list(request.GET)[0].replace('\'', '')
        try:
            parsed = json.loads(data)
            app_id = parsed['app_id']
            email = parsed['email']
            password = parsed['password']
            if check_app_id(app_id):
                if check_user_existence(email):
                    context_dict['out']['code'] = 0
                    context_dict['out']['token'] = generate_token()
                    user = find_user(email, password)
                    if not user:
                        context_dict['out']['code'] = 2
                        return JsonResponse(context_dict)
                    else:
                        session = find_session(user, app_id)
                        update_session(session, context_dict['out']['token'], app_id, email)
                else:
                    context_dict['out']['code'] = 3
            else:
                context_dict['out']['code'] = 2
        except ValueError:
            print("Decoding JSON failed.")
            context_dict['out']['code'] = 2
    else:
        context_dict['out']['code'] = 2
    return JsonResponse(context_dict)


def get_last_login(request):
    context_dict = {'out': {}}
    if request.method == 'GET':
        data = list(request.GET)[0].replace('\'', '')
        try:
            parsed = json.loads(data)
            token = parsed['token']
            app_id = parsed['app_id']
            email = parsed['email']
            if check_app_id(app_id):
                if check_user_existence(email):
                    if check_token_matching(token):
                        context_dict['out']['code'] = 0
                        context_dict['out']['date_time'] = get_date_time_login(token)
                    else:
                        context_dict['out']['code'] = 2
                else:
                    context_dict['out']['code'] = 3
            else:
                context_dict['out']['code'] = 2
        except ValueError:
            print("Decoding JSON failed.")
            context_dict['out']['code'] = 2
    else:
        context_dict['out']['code'] = 2
    return JsonResponse(context_dict)


def check_app_id(app_id):
    return App.objects.filter(pk=app_id).exists()


def generate_token():
    alphabet = string.ascii_letters + string.digits
    random = rand.SystemRandom()
    token = ''.join(random.choice(alphabet) for _ in range(10))
    return token


def save_new_user_to_db(email, name_surname, password):
    user = User(email=email, name_surname=name_surname, password=password)
    user.save()
    return user


def create_session(user, token, app_id):
    session = Session(user=user, token=token, app_id=app_id, time=datetime.date.today())
    session.save()
    return


def find_session(user, app_id):
    session = Session.objects.filter(user=user, app_id=app_id)[0]
    return session


def find_user(email, password):
    user = User.objects.filter(email=email, password=password)[0]
    return user


def update_session(session, token, app_id, email):
    user = User.objects.filter(email=email)[0]
    session.token = token
    session.user = user
    session.app_id = app_id
    session.save()
    return


def check_user_existence(email):
    return User.objects.filter(email=email).exists()


def check_token_matching(token):
    return Session.objects.filter(token=token).exists()


def get_date_time_login(token):
    return Session.objects.filter(token=token)[0].time.strftime("%B %d, %Y %H:%M:%S")
