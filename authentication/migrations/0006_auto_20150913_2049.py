# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0005_auto_20150913_2046'),
    ]

    operations = [
        migrations.RenameField(
            model_name='app',
            old_name='app_id',
            new_name='id',
        ),
    ]
