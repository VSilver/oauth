# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0004_auto_20150913_2020'),
    ]

    operations = [
        migrations.RenameField(
            model_name='app',
            old_name='id',
            new_name='app_id',
        ),
    ]
