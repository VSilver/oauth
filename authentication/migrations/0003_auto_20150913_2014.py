# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0002_auto_20150913_1832'),
    ]

    operations = [
        migrations.CreateModel(
            name='App',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('app_id', models.CharField(max_length=10)),
            ],
        ),
        migrations.AlterField(
            model_name='session',
            name='app_id',
            field=models.ForeignKey(to='authentication.App', related_name='sessions'),
        ),
    ]
