# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0003_auto_20150913_2014'),
    ]

    operations = [
        migrations.RenameField(
            model_name='session',
            old_name='app_id',
            new_name='app',
        ),
        migrations.RemoveField(
            model_name='app',
            name='app_id',
        ),
        migrations.AlterField(
            model_name='app',
            name='id',
            field=models.CharField(max_length=10, unique=True, primary_key=True, serialize=False),
        ),
    ]
