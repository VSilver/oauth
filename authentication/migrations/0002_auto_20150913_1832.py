# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='session',
            name='token',
            field=models.CharField(max_length=10),
        ),
        migrations.AlterField(
            model_name='session',
            name='user',
            field=models.ForeignKey(related_name='sessions', to='authentication.User'),
        ),
    ]
