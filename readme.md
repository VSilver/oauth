# Lab #0 Prototyping
------

For the implementation of this laboratory I've chosen the Django Framework 1.8.4 (Python 3.4.1). 

## Task #1

The server must be run with the following command:

``` shell
$python3 manage.py runserver
``` 

The the request must be of the form:

``` shell
$curl -X GET -H 'Content-Type: application/json' -d '{"app_id": "0", "email": "mail@mail.com", "password": "qwerty", "name_surname": "username"}' http://127.0.0.1:8000/register/
```

or a request inside the browser:
``` shell
http://127.0.0.1:8000/register?'{"app_id": "0", "email": "mail@mail.com", "password": "qwerty", "name_surname": "username"}'
```

## Task #2

### The list of questions generated while doing the Task #1:

1. Does the token change only at login or it must have a limited time for being available?
2. Can the user login several times from the same application?    
3. Can the user login simultaneously login from different applications and thus having all the sessions in the active mode?    
4. What happens if there is not explicitely created a GET request?
5. What happens if the data is wrong? (app_id/email/password/token)
6. How secure is our application?
7. Should we consider a POST request for transmitting the data?
8. Should we consider hashing for storing the passwords?

### The technical tasks implemented in order to make a well working app for oAuth system.

+ Models for storing the data of the application: 
    * User: name_surname, email, password
    * Application: app_id (a predefined list of applications which our system can recognize)
    * Session: user_id, app_id, token, time(of login)    
* For each API function (register/login/get_last_login) was created a function(view)
* First step for each - retrieving and parsing the input data
* Performing the checks of the user existence, email matching or token matching
* The uniqueness of the user email and the (email, app_id) touple is done at the application layer, inside views

### For future implementation

In order to make this application secure enough, the passwords may be hashed before storing. Also it may be added a predefined amount of time for which the token will be valid. This inserts one more functionality - the oAuth application must send a message to the requesting application the validity time of the token altogether with the token.

To be changed the output value from:
```
    {"token": "dsjn3Dsd", "code": "0"}
```
to 
```
    {"token": "dsjn3Dsd", "validity_minutes": "30", "code": "0"}
```
